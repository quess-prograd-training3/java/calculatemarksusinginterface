package com.example;

public class A implements Marks{
    protected double subject1;
    protected double subject2;
    protected double subject3;

    public A(double subject1, double subject2, double subject3) {
        this.subject1 = subject1;
        this.subject2 = subject2;
        this.subject3 = subject3;
    }
    @Override
    public double getPercentage(){
        return (subject1 + subject2 + subject3)/3.0;
    }
}
