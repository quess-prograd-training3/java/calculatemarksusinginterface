package com.example;

public class B implements Marks{
    protected double subject1;
    protected double subject2;
    protected double subject3;
    protected double subject4;

    public B(double subject1, double subject2, double subject3, double subject4) {
        this.subject1 = subject1;
        this.subject2 = subject2;
        this.subject3 = subject3;
        this.subject4 = subject4;
    }

    @Override
    public double getPercentage() {
        return (subject1+subject2+subject3+subject4)/4.0;
    }
}

